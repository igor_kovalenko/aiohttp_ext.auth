from __future__ import with_statement
import sys
from os import path
from alembic import context
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from logging.config import fileConfig

# Лайфхак нужный чтобы импорт отработал правильно
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from app import db, app

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def build_url():
    config = app['config']['gino']
    if config.get('dsn'):
        url = config['dsn']
    else:
        url = URL(
            drivername='postgresql',
            host=config.setdefault('host', 'localhost'),
            port=config.setdefault('port', 5432),
            username=config.setdefault('user', 'postgres'),
            password=config.setdefault('password', ''),
            database=config.setdefault('database', 'postgres'),
        )
    return url


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(
        url=build_url(), target_metadata=db, literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    url = build_url()
    connectable = create_engine(url)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=db
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
