from aiohttp import web
from aiohttp_ext.auth.decorators import bearer_authorization_required


@bearer_authorization_required(
    realm='http://authorization-point:8080/authorization-point/api/v1/token/',
    service='demo_app',
    scope='hello'
)
async def index(request):
    return web.json_response({'hello': f'Hello, {request["user"].username}!'})
