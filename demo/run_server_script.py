import logging
from aiohttp import web
from aio_manager import Command


class RunServer(Command):
    """
    Creates aiohttp server coroutine and executes event loop
    """
    def __init__(self, app):
        super().__init__('run_server', app)

    def run(self, app, args):
        logging.basicConfig(level=args.level)
        logging.getLogger().setLevel(args.level)

        web.run_app(app, host=args.host, port=args.port)

    def configure_parser(self, parser):
        super().configure_parser(parser)
        parser.add_argument('--host', metavar='HOST',
                            help='host or ip to listen on')
        parser.add_argument('--port', type=int, metavar='PORT',
                            help='port to listening')
        parser.add_argument('--level', type=str, metavar='LEVEL',
                            help='logging level')
        parser.set_defaults(host='127.0.0.1', port=5000, level='INFO')
