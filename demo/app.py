"""Демонстрационное приложение"""

from aiohttp import web
from gino.ext.aiohttp import Gino
from aiohttp_ext.auth.extension import Auth

from settings import settings
from views import index

db = Gino()

# Инициализируем объект приложения
app = web.Application(middlewares=[db])

# Добавляем в объект приложения настройки
app['config'] = settings

# Инициализируем GINO
db.init_app(app)

# Инициализируем расширение "Пользователи и авторизация"
# Расширение должно быть проинициализированно _после_ инициализации GINO
auth = Auth()
auth.init_app(app)

# Добавляем роутинг
app.router.add_get('/', index)
