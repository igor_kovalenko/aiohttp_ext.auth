#!/usr/bin/python3


from aio_manager.commands.runserver import RunServer as DeprecatedRunserver
from aio_manager import Manager
from aiohttp_ext.auth.script_commands import CreateSuperuser

from app import app
from run_server_script import RunServer

manager = Manager(app)

# Удаляем команду запуска работающую через deprecated механизм
# и вставляем вместо нее свою, актуальную
for i in range(0, len(manager.commands)-1):
    if manager.commands[i].__class__ == DeprecatedRunserver:
        manager.commands.pop(i)
manager.add_command(RunServer(app))

manager.add_command(CreateSuperuser(app))

# manager.add_command("migrate", ManageMigrations())


if __name__ == "__main__":
    manager.run()
