# Демонстрационное приложение

## Установка

Перед началом установки создаем файл настроек из шаблона и, по необходимости, корректируем их
```bash
cp settings.py.tmpl settings.py
```

Создаем виртуальное окружение

```bash
virtualenv env
source env/bin/activate
```

Устанавливаем необходимые пакеты

```bash
pip install -r requirements.txt
```

Выполняем миграции БД

```bash
alembic upgrade head
```

## Запуск

```bash
python manage.py run_server
```
